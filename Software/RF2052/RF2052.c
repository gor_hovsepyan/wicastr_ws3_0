/**********************************************
*文件名：REG2052
*功  能：对RFMD2052芯片进行配置
*作  者：troy
*时  间：2011.3.10
***********************************************/



#include<pic.h>					//PIC单片机包含的头文件
#include<RF2052.h>				//RF2052寄存器定义的头文件
#define uchar unsigned char
#define uint unsigned int
#define write 0x01				//宏定义0x01为写数操作
#define ENX   RA0 				//定义端口
#define SCLK  RA1
#define SDATA RA2


#define  ENBL 	RC0
#define  RESETB RC1
#define  MODE   RC2
          
__CONFIG(0X3F31);				//0011 1111 0011 0001				//pic单片机配置字节设置  /
								//bit 13 	1=Code protection off
								//bit 12 	read as '1'
								//bit 11  	1 = In-Circuit Debugger disabled,
								//bit 9-10  11=Write protection off; all program memory may be written to by EECON control 
 								//bit 8    	1=Data EEPROM code protection off
                                //bit 7    	0=low-voltage programming enabled
								//bit 6    	0=Brown-out Reset Disable 
								//bit 5-4  	read as '1'
								//bit 3   	0=Power-up Timer Enable
								//bit 2   	0=Watchdog Timer Disable  
								//bit1-0  	11 = RC oscillator 
											//10 = HS oscillator 
											//01 = XT oscillator 
											//00 = LP oscillator


/*********************************************
*函数名：Delay
*功  能：延时M毫秒程序
*输  入：uint xms
*输  出：--
**********************************************/
void delay(uint xms)			
{
	uint i,j;
	for(i=xms;i>0;i--)
	for(j=110;j>0;j--);
}



/*********************************************
*函数名：Delaynop
*功  能：延时微秒程序
*输  入：uint x，uchar y
*输  出：--
**********************************************/

void delaynop()		//延时220us
{
	uint i;
	for(i=100;i>0;i--)
	asm("nop");
}


/*******************************************
*函数名：inint
*功能：初始化PIC16F877A单片机寄存器端口
*输入：--
*输出：--
*******************************************/
void inint()
{
	ADCON1=0X06;            //所有IO均为数字口，模拟量输入禁止
	OPTION=0x80;        	//关闭RB口电平变化功能
	TRISA=0;
	TRISB=0;
	TRISC=0;
	TRISD=0;				//定义每个寄存器端口为输出
	
	
	PORTA=0;				//初始化每个端口为低电平，没有输出
	PORTB=0;
	PORTC=0;
	PORTD=0;
}




/*********************************************
*函数名：Ini2052
*功  能：对配置前进行初始化
*输  入：--
*输  出：--
**********************************************/
void Ini2052()
{
 	ENX=1;           //SPI初始化
	SCLK=0;
	SDATA=0;

	ENBL=0;          //对2052配置前，芯片不进行工作
	MODE=0;         //选择path1
	RESETB=1;         //初始复位引脚为高，复位需要至少100ns低脉冲

	//Set SDATA as output
}




/*********************************************
*函数名：RESETB2052
*功  能：对2052进行复位，复位后寄存器恢复默认值
*输  入：--
*输  出：--
**********************************************/
void RESETB2052()
{
	RESETB=0;        	//2052复位需要至少100ns低脉冲
	delaynop();

  
    RESETB=1;
	delaynop(); //  延时220us Reset delay time  >5ns
}




/********************************************
*函数名：Send_Add
*功  能：发送寄存器地址
*输  入：uchar R_W  , uchar addr
*输  出：--
*********************************************/
void Send_Add(uchar R_W,uchar addr)
{
	uchar count;
	
	SCLK=1;
	delaynop();            //发送初始第一个时钟脉冲
	SCLK=0;

	ENX=0;


    SCLK=1;
	delaynop();             //开始读写位操作
	SCLK=0;					//第二个时钟为初始化

						    //开始送入读写R_W，第三个时钟上升沿读数
	if (R_W==0x01)        	//设置是否为写操作
	   {
			
			SDATA=0;
			
			delaynop();
			SCLK=1;
			delaynop();
			SCLK=0;
		}
	else if (R_W==0x02)    //设置为读操作
		{	
		
			SDATA=1;
			
			delaynop();
			SCLK=1;
			delaynop();
			SCLK=0;
		}
    count=0;
	while(count<7)
	{
		if(addr&(0x40>>count))   //从地址里提取数据位发送
		{
		
			SDATA=1;
			delaynop();
			SCLK=1;
			delaynop();
			SCLK=0;
		}
		else
		{
			
			SDATA=0;
			
			delaynop();
			SCLK=1;
			delaynop();
			SCLK=0;
		}
		count++;
	}

	if (R_W==0x02)           //如果为读操作则送完地址后延时一个半时钟后取数       
	{

		delaynop();
        SCLK=1;
  	    SDATA=1;//Set SDATA as input
		delaynop();
	
		SCLK=0;
		delaynop();
		SCLK=1;
	}
}




/***********************************************
*函数名：Send_data
*功  能：送入16位寄存器数据
*输  入：uchar data_16
*输  出：
************************************************/
void Send_data(uint data_16)
{
	uchar count=0;
	
	while(count<16)    //先送入高8位数据
	{
		if(data_16&(0x8000>>count))     //提取数据位发送
		{	
		
			SDATA=1;

			delaynop();
			SCLK=1;
			delaynop();
			SCLK=0;
		}
		else
		{
			
			SDATA=0;

			delaynop();
			SCLK=1;
			delaynop();
			SCLK=0;			
		}
		count++;
	}
		
	ENX=1;
	delaynop();                         //控制端升高，数据发送结束
	SCLK=1;                        		//发送最后一个脉冲后，数据将被送入相应寄存器
	delaynop();
	SCLK=0;

}


/*********************************************
*函数名：Set_path
*功  能：选择通道1还是2
*输  入：uchar WhichPath
*输  出：--
**********************************************/
void Set_path( uchar WhichPath)
{
	MODE=WhichPath;                   //bit 0为1通道，bit 1为2通道  
}






/*********************************************
*函数名：Enable
*功  能：芯片使能
*输  入：--
*输  出：--
**********************************************/
void Enable()
{
	ENBL=1;	
	
}



/*********************************************
*函数名：主函数
*功  能：
**********************************************/
void main()
{
	inint();				//初始化PIC单片机端口

	delay(1000);         	//延时1S 

	Ini2052();             	//初始化RF2052控制端口
	delay(100);				
 	while(1)
	{
  
	 	RESETB2052();
		delay(10);
		Send_Add(write,Add_CFG1);
		Send_data(0x91A0);
		delay(800);
   		Send_Add(write,Add_CFG2);
		Send_data(0x8C5B);
		delay(800);
		Send_Add(0x01,Add_CFG3);
		Send_data(0x407);
		delay(800);
		Send_Add(0x01,Add_CFG4);
		Send_data(0x140F);
		delay(800);		
		Send_Add(0x01,Add_CFG5);
		Send_data(0xC04);
		delay(800);
		Send_Add(0x01,Add_CFG6);
		Send_data(0x100);
 		delay(800);  

		Send_Add(0x01,Add_PLL20);
		Send_data(0x301F);
		delay(800);
		Send_Add(0x01,Add_PLL21);
		Send_data(0x0);
		delay(800);
		Send_Add(0x01,Add_PLL22);
		Send_data(0x7E);
		delay(800);
		Send_Add(0x01,Add_PLL23);
		Send_data(0x6402);
		delay(800);
		Send_Add(0x01,Add_PLL24);
		Send_data(0x14E4);
		delay(800);
		Send_Add(0x01,Add_PLL25);
		Send_data(0x10);
    	delay(1000);
		delay(1);
		Set_path(1);
		delay(1000);
		Enable();
    	delay(1000);
		PORTB=0XFF;			//B端口的灯全亮，表示送数以完成
	

		
	}
		

	 	//while(1);
 	
}
