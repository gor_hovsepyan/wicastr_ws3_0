
#ifndef __REG2052_H__
#define __REG2052_H__

#define Add_CFG1 0x00
#define Add_CFG2 0x01
#define Add_CFG3 0x02
#define Add_CFG4 0x03
#define Add_CFG5 0x04
#define Add_CFG6 0x05

#define Add_PLL10 0x08
#define Add_PLL11 0x09
#define Add_PLL12 0x0A
#define Add_PLL13 0x0B
#define Add_PLL14 0x0C
#define Add_PLL15 0x0D


#define Add_PLL20 0x10
#define Add_PLL21 0x11
#define Add_PLL22 0x12
#define Add_PLL23 0x13
#define Add_PLL24 0x14
#define Add_PLL25 0x15

#define Add_GP0     0x18

#define Add_CHIPREV 0x19

#define Add_RB1     0x1C
#define Add_RB2     0x1D
#define Add_RB3     0x1E

#define Add_TEST    0X1F

/*CFG1 Opertional Configuration Parameters*/
//BIT NAME   Default      Function 
//15  LD_EN    1          Enable lock detector circuitry 
//14  LD_LEV   0          Modify lock range for lock detector
//13  TVCO(4:0)0          VCO warm-up time=TVCO/(FREF*256)
//12           0 
//11           0
//10           0 
//9            0  
//8   PDP      1          Phase detector polarity: 0=positive, 1=negative    
//7   LF_ACT   1          Active loop filter enable, 1=Active 0=Passive
//6   CPL(1:0) 1          Charge pump leakage current: 00=no leakage, 01=low leakage, 10=mid leakage, 11=high
//5            0          leakage  
//4   CT_POL   0          Polarity of VCO coarse-tune word: 0=positive, 1=negative
//3            0
//2   EXT_VCO  0          0=Normal operation 1=external VCO (VCO3 disabled, KV_CAL and CT_CAL must be disabled)
//1   FULLD    0          0=Half duplex, mixer is enabled according to MODE pin, 1=Full duplex, both mixers enabled
//0   CP_LO_l  0          0=High charge pump current, 1=low charge pump current
unsigned char CFG1_L;
unsigned char CFG1_H;

/*CFG2 Mixer Bias and PLL Calibration*/
//BIT NAME       Default      Function 
//15  MIX1_IDD   1            This register is not used for the RF2052.
//14             0
//13             0
//12  MIX1_VB    0            This register is not used for the RF2052. 
//11             1
//10  MIX2_IDD   1            Mixer 2 current setting: 000=0mA to 111=35mA in 5mA steps
//9              0
//8              0
//7   MIX2_VB    0
//6              1            Mixer 2 voltage bias  
//5              0
//4   KV_RNG     1            Sets accuracy of voltage measurement during KV calibration: 0=8bits, 1=9bits
//3   NBR_CT_AVG 1            Number of averages during CT cal
//2              0
//1   NBR_KV_AVG 0            Number of averages during KV cal
//0              0
unsigned char CFG2_L;
unsigned char CFG2_H;


/*CFG3 PLL Calibration*/
//BIT NAME       Default      Function 
//15  TKV1       0            Settling time for first measurement in LO KV compensation
//14             0 
//13             0
//12             0
//11  TKV2       0            Settling time for second measurement in LO KV compensation
//10             1
//9              0
//8              0
//7              0
//6              0  
//5              0
//4              0 
//3   FLL_FACT   0            Default setting 01. Needs to be set to 00 for N<28. This case can arise when higher phase
//2              1            detector frequencies are used.
//1   CT_CPOL    0
//0   REFSTBY    0            Reference oscillator standby mode 0=XO is off in standby mode, 1=XO is on in standby mode 
unsigned char CFG3_L;
unsigned char CFG3_H;


/*CFG4 Crystal Oscillator and Reference Divider*/
//BIT NAME       Default      Function
//15  CLK_DIV        0        Reference divider, divide by 2 (010) to 7 (111) when reference divider is enabled
//14                 0
//13                 0          
//12  CLK_DIV_BYPASS 1        Reference divider enabled=0, divider bypass (divide by 1)=1                 
//11  XO_CT          1        Crystal oscillator coarse tune (approximately 0.5pF steps from 8pF to 16pF)         
//10                 0  
//9                  0
//8                  0
//7   XO_l2          0        Crystal oscillator current setting
//6   XO_l1          0   
//5   XO_CR_S        0        Crystal oscillator additional fixed capacitance (approximately 0.25pF) 
//4   TCT            0        Duration of coarse tune acquisition
//3                  1
//2                  1
//1                  1
//0                  1  
unsigned char CFG4_L;
unsigned char CFG4_H;


/*CFG5 LO Bias*/
//BIT NAME       Default      Function
//15  LO1_l      0            Local oscillator Path1 current setting
//14             0           
//13             0              
//12             0           
//11  LO2_l      0            Local oscillator Path2 current setting               
//10             0     
//9              0    
//8              0    
//7   T_PH_ALGN  0            Phase alignment timer            
//6              0  
//5              0    
//4              0       
//3              0    
//2              1    
//1              0    
//0              0   
unsigned char CFG5_L;
unsigned char CFG5_H;

/*CFG6 Start-up Timer*/
//BIT NAME       Default      Function
//15  SU_WAIT    0            Crystal oscillator settling timer.  
//14             0           
//13             0              
//12             0           
//11             0                       
//10             0     
//9              0    
//8              1    
//7              0                     
//6              0  
//5              0    
//4              0       
//3              0    
//2              1    
//1              0    
//0              0  
unsigned char CFG6_L;
unsigned char CFG6_H;


/*PLL1x0 (08h) - VCO, LO Divider and Calibration Select*/
//BIT NAME       Default      Function
//15  P1_VCOSEL  0            Path 1 VCO band select: 00=VCO1, 01=VCO2, 10=VCO3, 11=Reserved
//14             1
//13  P1_CT_EN   1            Path 1 VCO coarse tune: 00=disabled, 11=enabled
//12             1
//11  P1_KV_EN   0            Path 1 VCO tuning gain calibration: 00=disabled, 11=enabled
//10             0
//9   P1_LODIV   0            Path 1 local oscillator divider: 00=divide by 1, 01=divide by 2, 10=divide by 4, 11=reserved
//8              1
//7              0 
//6              0
//5   P1_CP_DEF  0            Charge pump current setting
//4              1            If P1_KV_EN=11 this value sets charge  pump current during KV compensation only
//3              1 
//2              1
//1              1
//0              1
unsigned char    PLL10_L ;
unsigned char    PLL10_H ;

/*PLL1x1 (09h) - MSB of Fractional Divider Ratio*/ 
//BIT NAME       Default      Function
//15  P1_NUM_MSB 0            Path 1 VCO divider numerator value, most significant 16 bits
//14             1
//13             1
//12             0
//11             0 
//10             0
//9              1
//8              0
//7              0 
//6              1 
//5              1
//4              1
//3              0 
//2              1
//1              1
//0              0
unsigned char    PLL11_L ;
unsigned char    PLL11_H ;

/*PLL1x2 (0Ah) - LSB of Fractional Divider Ratio and CT Default*/
//BIT NAME       Default      Function
//15 P1_NUM_LSB  0            Path 1 VCO divider numerator value, least significant 8 bits
//14             0
//13             1
//12             0
//11             0 
//10             1
//9              1
//8              1
//7  P1_CT_DEF   0            Path 1 VCO coarse tuning value, used when P1_CT_EN=00
//6              1
//5              1
//4              1
//3              1 
//2              1
//1              1
//0              0
unsigned char    PLL12_L ;
unsigned char    PLL12_H ;

/*PLL1x3 (0Bh) - Integer Divider Ratio and VCO Current*/
//BIT NAME       Default      Function
//15  P1_N       0            Path 1 VCO divider integer value
//14             0 
//13             1
//12             0
//11             0 
//10             0
//9              1
//8              1
//7              0 
//6              0
//5              0
//4              0
//3              0 
//2              0             Path 1 VCO bias setting: 000=minimum value, 111=maximum value
//1              1
//0              0
unsigned char    PLL13_L ;
unsigned char    PLL13_H ;


/*PLL1x4 (0Ch) - Calibration Settings*/
//BIT NAME       Default      Function
//15  P1_DN      0            Path 1 frequency step size used in VCO tuning gain calibration
//14             0
//13             0
//12             1
//11             0 
//10             1
//9              1
//8              1
//7              1 
//6   P1_CT_GAIN 1            Path 1 coarse tuning calibration gain
//5              1
//4              0
//3   P1_KV_GAIN0             Path 1 VCO tuning gain calibration gain
//2              1
//1              0
//0              0
unsigned char    PLL14_L; 
unsigned char    PLL14_H;


/*PLL1x5 (0Dh) - More Calibration Settings*/
//BIT NAME          Default      Function
//15    P1_N_PHS_ADJ  0            Path 1 frequency step size used in VCO tuning gain calibration
//14                  0
//13                  0
//12                  0
//11                  0 
//10                  0
//9                   0
//8                   0
//7                   0 
//6                   0
//5                   0  
//4     P1_CT_V       1            Path 1 course tuning voltage setting when performing course tuning calibration. Default
//3                   0            value is 16. Change to 12 when using VCO1 for frequencies above 2.2GHz.
//2                   0
//1                   0
//0                   0
unsigned char    PLL15_L; 
unsigned char    PLL15_H;


/*PLL2x0  - VCO, LO Divider and Calibration Select*/
//BIT NAME       Default      Function
//15  P2_VCOSEL  0            Path 2 VCO band select: 00=VCO1, 01=VCO2, 10=VCO3, 11=Reserved
//14             1
//13  P2_CT_EN   1            Path 2 VCO coarse tune: 00=disabled, 11=enabled
//12             1
//11  P2_KV_EN   0            Path 2 VCO tuning gain calibration: 00=disabled, 11=enabled
//10             0
//9   P2_LODIV   0            Path 2 local oscillator divider: 00=divide by 1, 01=divide by 2, 10=divide by 4, 11=reserved
//8              1
//7              0 
//6              0
//5   P2_CP_DEF  0            Charge pump current setting
//4              1            If P2_KV_EN=11 this value sets charge  pump current during KV compensation only
//3              1 
//2              1
//1              1
//0              1
unsigned char    PLL20_L; 
unsigned char    PLL20_H;

/*PLL2x1  - MSB of Fractional Divider Ratio*/ 
//BIT NAME       Default      Function
//15  P2_NUM_MSB 0            Path 2 VCO divider numerator value, most significant 16 bits
//14             1
//13             1
//12             0
//11             0 
//10             0
//9              1
//8              0
//7              0 
//6              1 
//5              1
//4              1
//3              0 
//2              1
//1              1
//0              0
unsigned char    PLL21_L ;
unsigned char    PLL21_H ; 

/*PLL2x2  - LSB of Fractional Divider Ratio and CT Default*/
//BIT NAME       Default      Function
//15 P2_NUM_LSB  0            Path 2 VCO divider numerator value, least significant 8 bits
//14             0
//13             1
//12             0
//11             0 
//10             1
//9              1
//8              1
//7  P2_CT_DEF   0            Path 2 VCO coarse tuning value, used when P1_CT_EN=00
//6              1
//5              1
//4              1
//3              1 
//2              1
//1              1
//0              0
unsigned char    PLL22_L ;
unsigned char    PLL22_H ;

/*PLL2x3  - Integer Divider Ratio and VCO Current*/
//BIT NAME       Default      Function
//15  P2_N       0            Path 2 VCO divider integer value
//14             0 
//13             1
//12             0
//11             0 
//10             0
//9              1
//8              1
//7              0 
//6              0
//5              0
//4              0
//3              0 
//2  P2_VCOl     0             Path 2 VCO bias setting: 000=minimum value, 111=maximum value
//1              1
//0              0
unsigned char    PLL23_L ;
unsigned char    PLL23_H ;


/*PLL2x4  - Calibration Settings*/
//BIT NAME       Default      Function
//15  P2_DN      0            Path 2 frequency step size used in VCO tuning gain calibration
//14             0
//13             0
//12             1
//11             0 
//10             1
//9              1
//8              1
//7              1
//6   P2_CT_GAIN 1            Path 2 coarse tuning calibration gain
//5              1
//4              0
//3   P2_KV_GAIN 0            Path 2 VCO tuning gain calibration gain
//2              1
//1              0
//0              0
unsigned char    PLL24_L ;
unsigned char    PLL24_H ;


/*PLL1x5  - More Calibration Settings*/
//BIT NAME          Default      Function
//15  P2_N_PHS_ADJ    0          Path 2 synthesizer phase adjustment
//14                  0
//13                  0
//12                  0
//11                  0 
//10                  0
//9                   0
//8                   0
//7                   0 
//6                   0
//5                   0  
//4   P2_CT_V         1           Path 2 course tuning voltage setting when performing course tuning calibration. Default
//3                   0           value is 16. Change to 12 when using VCO1 for frequencies above 2.2GHz.
//2                   0
//1                   0
//0                   0
unsigned char    PLL25_L ;
unsigned char    PLL25_H ;


/*GPO (18h) - Internal Control Output Settings*/
//BIT NAME          Default      Function
//15                0 
//14  P1_GPO1       0            Setting of GPO1 when path 1 is active, used internally only
//13                0
//12  P1_GPO3       0            Setting of GPO3 when path 1 is active, used internally only
//11  P1_GPO4       0            Setting of GPO4 when path 1 is active, used internally only
//10                0
//9                 0
//8                 0
//7                 0 
//6   P2_GPO1       0            Setting of GPO1 when path 2 is active, used internally only
//5                 0
//4   P2_GPO3       0            Setting of GPO3 when path 2 is active, used internally only
//3   P2_GPO4       0            Setting of GPO4 when path 2 is active, used internally only
//2                 0
//1                 0
//0                 0
unsigned char GPO_L;
unsigned char GPO_H;
 
/*CHIPREV (19h) - Chip Revision Information*/
//BIT NAME          Default      Function
//15  PARTNO        0            RFMD Part number for device
//14                0
//13                0
//12                0
//11                0  
//10                0
//9                 0
//8                 0
//7   REVNO         X            Part revision number
//6                 X
//5                 X
//4                 X
//3                 X 
//2                 X
//1                 X
//0                 X
unsigned char CHIPREV_L;
unsigned char CHIPREV_H;

/*RB1 (1Ch) - PLL Lock and Calibration Results Read-back*/
//BIT NAME          Default      Function
//15  LOCK          X            PLL lock detector, 0=PLL locked, 1=PLL unlocked
//14  CT_CAL        X            CT setting (either result of course tune calibration, or CT_DEF, depending on state of CT_EN).
//13                X            Also depends on the MODE of the device
//12                X
//11                X 
//10                X
//9                 X
//8                 X
//7   CP_CAL        X            CP setting (either result of KV cal, or CP_DEF, depending on state of KV_EN).
//6                 X            Also depends on the MODE of the device
//5                 X
//4                 X
//3                 X 
//2                 X
//1                 0
//0                 0
unsigned char RB1_L;
unsigned char RB1_H;


/*Calibration Results Read-Back*/
//BIT NAME          Default      Function
//15  VO_CAL        X            The VCO voltage measured at the start of a VCO gain calibration
//14                X
//13                X
//12                X
//11                X 
//10                X
//9                 X
//8                 X
//7   V1_CAL        X            The VCO voltage measured at the end of a VCO gain calibration
//6                 X
//5                 X
//4                 X
//3                 X 
//2                 X
//1                 X
//0                 X
unsigned char RB2_L;
unsigned char RB2_H;

/*RB3 (1Eh) - PLL state Read-Back*/
//BIT NAME          Default      Function
//15  RSM_STATE     X            State of the radio state machine
//14                X
//13                X
//12                X
//11                X 
//10                X
//9                 0
//8                 0
//7                 0 
//6                 0
//5                 0
//4                 0
//3                 0 
//2                 0
//1                 0
//0                 0
unsigned char RB3_L;
unsigned char RB3_H;

/*TEST (1Fh) - Test Modes*/
//BIT NAME          Default      Function
//15  TEN           0            Enables test mode
//14  TMUX          0            Sets test multiplexer state
//13                0
//12                0
//11  CPU           0            Set charge pump to pump up, 0=normal operation 1=pump down
//10  CPD           0            Set charge pump to pump down, 0=normal operation 1=pump down
//9   FNZ           0            0=normal operation, 1=fractional divider modulator disabled
//8   LDO_BYP       0            On chip low drop out regulator bypassed
//7   TSEL          0 
//6                 0
//5                 0
//4   DACTEST       0            DAC test
//3                 0 
//2                 0
//1                 0
//0                 0

unsigned char TEST_L;
unsigned char TEST_H;


#endif